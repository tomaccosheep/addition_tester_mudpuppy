# Type-along: Addition Tester (Lab 12 hint)

This repo is an attempt to have a type-along session. You can start by using git clone, like so:

```git clone https://tomaccosheep@bitbucket.org/tomaccosheep/addition\_tester\_mudpuppy.git  addition\_tester\_copy1```

If you fall behind and we can't figure out why your program doesn't work, then make a copy of the most updated version with this:

```git clone https://tomaccosheep@bitbucket.org/tomaccosheep/addition\_tester\_mudpuppy.git  addition\_tester\_copy2```
