function randint(lowerNum, upperNum) {
    return lowerNum + Math.floor(
        Math.random() * ((upperNum + 1) - lowerNum)
    )
}
let additionDiv = document.querySelector("#addition-div")
let correctDiv = document.querySelector("#correct-div")
let numText = document.querySelector("#num-text")
let guessButton = document.querySelector("#guess-button")
let num1 = randint(1, 5)
let num2 = randint(1, 5)
additionDiv.innerText = `What is ${num1} + ${num2}`
guessButton.onclick = function() { // this function will run when someone clicks the button
    if (parseInt(numText.value) === num1 + num2) {
        correctDiv.innerText = "Correct!"
    } else {
        correctDiv.innerText = "You're wrong, stupid!"
    }
}